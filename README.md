# BIM & Scan� Third-Party Library (nanoFLANN)

![BIM & Scan](BimAndScan.png "BIM & Scan� Ltd.")

Conan build script for [nanoFLANN](https://github.com/jlblancoc/nanoflann/), a C++11 header-only library for Nearest-Neighbour (NN) search with KD-trees.

Supports version 1.3.0 (stable).

Requires a C++11 compiler (or higher).
