/*
 * 2018-2019 © BIM & Scan® Ltd.
 * See 'README.md' in the project root for more information.
 */
#include <cstdlib>
#include <iostream>

#include <nanoflann.hpp>


int main(int p_arg_count,
         char** p_arg_vector)
{
    std::cout << "'nanoFLANN' package test (compilation, linking, and execution).\n";

    // only need to check include

    std::cout << "'nanoFLANN' package works!" << std::endl;
    return EXIT_SUCCESS;
}
