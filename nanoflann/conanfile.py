#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# 2018-2019 � BIM & Scan� Ltd.
# See 'README.md' in the project root for more information.
#
import os

from conans.model.conan_file import ConanFile
from conans.errors import ConanInvalidConfiguration
from conans import tools


class NanoFLANN(ConanFile):
    name = "nanoflann"
    version = "1.3.0"
    license = "BSD-2-Clause"
    url = "https://bitbucket.org/headcount_bimandscan/conan-nanoflann"
    description = "A C++11 header-only library for Nearest-Neighbour (NN) search with KD-trees."
    generators = "txt"
    author = "Neil Hyland <neil.hyland@bimandscan.com>"
    homepage = "https://github.com/jlblancoc/nanoflann"
    no_copy_source = True

    _src_dir = f"{name}-{version}"

    settings = "compiler"

    exports = "../LICENCE.md"

    def _valid_cppstd(self):
        return self.settings.compiler.cppstd == "17" or \
               self.settings.compiler.cppstd == "gnu17" or \
               self.settings.compiler.cppstd == "20" or \
               self.settings.compiler.cppstd == "gnu20" or \
               self.settings.compiler.cppstd == "11" or \
               self.settings.compiler.cppstd == "gnu11" or \
               self.settings.compiler.cppstd == "14" or \
               self.settings.compiler.cppstd == "gnu14"

    def config_options(self):
        if not self._valid_cppstd():
            raise ConanInvalidConfiguration("Library requires C++11 or higher!")

    def source(self):
        zip_name = f"{self._src_dir}.zip"

        tools.download(f"https://github.com/jlblancoc/nanoflann/archive/v{self.version}.zip",
                       zip_name)

        tools.unzip(zip_name)
        os.unlink(zip_name)

    def package(self):
        self.copy("nanoflann.hpp",
                  "include",
                  f"{self._src_dir}/include")

        self.copy("COPYING",
                  "licenses",
                  self._src_dir)

    def package_id(self):
        self.info.header_only()
